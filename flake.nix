{
  description = "vedirect-reader is a Prometheus exporter for VE.Direct devices";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
        pkgs = import nixpkgs {
          inherit system;
        };
      in rec
      {
        # Nix formatter, run using `$ nix fmt`
        formatter = pkgs.alejandra;

        # Exports the vedirect-reader package
        # Build with `$ nix build`
        packages.default = pkgs.callPackage ./default.nix {};

        nixosModules.default = ./module.nix;

        # Creates a shell with the necessary dependencies
        # Enter using `$ nix develop`
        # devShell = pkgs.mkShell {
        #   buildInputs = with pkgs; [
        #     nodejs_20
        #   ];
        # };
      });
}
