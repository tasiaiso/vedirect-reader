import { State } from "./consumer.js";

/**
 *
 * @param state
 * @returns
 */
export function buildMetrics(state: State): string {
	return `
# HELP volt_batt battery voltage
# TYPE volt_batt gauge
volt_batt ${state.inverter.batteryVoltage}
# HELP volt_out battery voltage TODOC
# TYPE volt_out gauge
volt_out ${state.inverter.lineVoltage}
# HELP volt_batt battery voltage TODOC
# TYPE volt_batt gauge
amp_out_inv ${state.inverter.lineCurrent}`;
}
