import * as consumer from "./consumer.js";
import * as server from "./http.js";

consumer.subscribe(["/dev/ttyUSB0", "/dev/ttyUSB1"]);

server.start(8080);
