import * as http from "node:http";
import * as prom from "./prometheus.js";
import * as consumer from "./consumer.js";

export function start(port: number) {
	http.createServer(requestHandler).listen(port);
}

function requestHandler(
	req: http.IncomingMessage,
	res: http.ServerResponse<http.IncomingMessage> & { req: http.IncomingMessage }
) {
	if (req.url == "/metrics") {
		// console.log(consumer.getState());

		res.write(prom.buildMetrics(consumer.getState()));
	} else {
		res.write("vedirect-reader\n\nGo to /metrics");
	}
	res.end();
}
