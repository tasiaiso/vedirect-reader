// @ts-ignore
import VEDirect from "@signalk/vedirect-serial-usb/standalone.js";

/**
 *
 * @param device
 * @param subscriptionCallback
 */
export function subscribe(devices = ["/dev/ttyUSB0"]) {
	for (const device of devices) {
		const consumer = new VEDirect({
			device: device,
			ignoreChecksum: true,
			mainBatt: "House",
			auxBatt: "Starter",
			solar: "Main",
		});

		consumer.start();

		consumer.on("delta", (delta: object) => {
			//@ts-ignore
			// console.log("d", JSON.stringify(delta.updates[0].values));
			// console.log(deltaQueue)
			// deltaQueue.push(delta);
			if(deltaQueue.length > 5) deltaQueue.shift()
		});
	}
}
const deltaQueue: object[] = [];

const state = {
	inverter: {
		mode: "",
		lineVoltage: 0,
		lineCurrent: 0,
		batteryVoltage: 0,
	},
	charger: {
		mode: "",
		batteryVoltage: 0,
		batteryCurrent: 0,
		batteryPanelVoltage: 0,
		batteryPanelCurrent: 0,
		// more
	},
};
export type State = typeof state;

export function getState() {
	return parseDelta(deltaQueue, state);
}

function parseDelta(deltas: object[], target: State): State {
	for (const delta of deltas) {
		let isCharger = false;
		// console.log(delta);
	
		// @ts-ignore
		for (const key of delta.updates[0].values) {
			if (key.path == "electrical.solar.Main.panelVoltage") isCharger = true;
		}
		// differentiate between the two
		// @ts-ignore
		for (const key of delta.updates[0].values) {
			const value = key.value;
	
			if (key.path == "electrical.charger.House.chargingMode") {
				if (isCharger) target.charger.mode = value;
				else target.inverter.mode = value;
			}
	
			if (key.path == "electrical.batteries.House.voltage") {
				if (isCharger) target.charger.batteryVoltage = value;
				else target.inverter.batteryVoltage = value;
			}
	
			// Inverter
			if (key.path == "electrical.ac.victronDevice.phase.A.lineLineVoltage")
				target.inverter.lineVoltage = value;
	
			if (key.path == "electrical.ac.victronDevice.phase.A.current")
				target.inverter.lineCurrent = value;
	
			if (key.path == "electrical.batteries.House.voltage")
				target.inverter.batteryVoltage = value;
	
			// Charger
			if (key.path == "electrical.batteries.House.current")
				target.charger.batteryCurrent = value;
	
			if (key.path == "electrical.solar.Main.panelVoltage")
				target.charger.batteryPanelVoltage = value;
	
			if (key.path == "electrical.solar.Main.panelPower")
				target.charger.batteryPanelCurrent = value;
		}
	}
	return state;
}
