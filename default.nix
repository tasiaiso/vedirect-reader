{
  pkgs ? import <nixpkgs> {},
  lib ? import <nixpkgs/lib>,
}:
pkgs.buildNpmPackage rec {
  pname = "vedirect-reader";
  version = "0.0.0";

  src = pkgs.fetchFromGitea {
    domain = "gitea.com";
    owner = "tasiaiso";
    repo = "vedirect-reader";
    rev = "f559011de0c1b14bfb51cc9a88eddaa263934349";
    hash = "sha256-NZA3uuEy9/Vlz9zgpx4n/621xFmCmNUpwOnTPB1icQ0=";
  };

  npmDepsHash = "sha256-FzZwdsqr0al6FwGdin7LMmqFSA5u4OPaNVOdT5ZS8zw=";
}
