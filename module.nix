{
  config,
  lib,
  pkgs,
  outputs,
  ...
}: let
  cfg = config.services.vedirect-reader;
in {
  options.services.vedirect-reader = {
    enable = lib.mkEnableOption "vedirect-reader";
  };

  config = lib.mkIf cfg.enable {
    systemd.services.vedirect-reader = {
      wantedBy = ["multi-user.target"];
      serviceConfig.ExecStart = "${pkgs.nodejs_20}/bin/node /nix/store/3x3g95d5n528d386kw6bi9dvzrdm54ys-vedirect-reader-0.0.0/lib/node_modules/vedirect-reader/out/main.js";
    };
  };
}
